// Miniactivity
db.fruits.insertMany([
	{
	name: "apple",
	color: "red",
	stock: 12,
	price: 20,
	supplier_id: "1",
	onSale: true,
	origin: ["Philippines", "Japan"]
	},
	{
	name: "kiwi",
	color: "green",
	stock: 10,
	price: 18,
	supplier_id: "2",
	onSale: false,
	origin: ["Philippines", "Japan"]
	},
	{
	name: "mango",
	color: "yellow",
	stock: 200,
	price: 45,
	supplier_id: "1",
	onSale: false,
	origin: ["Philippines", "Japan"]
	},
	{
	name: "banana",
	color: "yellow",
	stock: 100,
	price: 50,
	supplier_id: "4",
	onSale: true,
	origin: ["Philippines", "Japan"]
	}
]);

========================

// Aggregate, $match
db.fruits.aggregate([
    {$match: {onSale: true}}
])

// update your documents and make only two supplier id : supplier id 1 and 2

db.fruits.updateOne(
    {name: "banana"},
    {
        $set: {
            supplier_id: "2"
        }
    }
 );

// Aggregate, $group
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}
])

// Aggregate, $project
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$project: {_id: 0, total: 1}}
])

// Aggregate, $sort; 1 = smallest to largest
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total: 1}}
]) 

// Aggregate, $sort; -1 = reverse order
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total: -1}}
])

// $unwind - deconstructs an array field & displays separate document
db.fruits.aggregate([
{$unwind: "$origin"}
])

// $unwind + $group + kinds
db.fruits.aggregate([
{$unwind: "$origin"},
{$group: {_id: "$origin", kinds: {$sum: 1}}}
])

// $unwind + $match + $group + kinds ; displays JAPAN only
db.fruits.aggregate([
{$unwind: "$origin"},
{$match: {origin: "Japan"}},
{$group: {_id: "$origin", kinds: {$sum: 1}}}
])

/*	
	Miniactivity

	create owners collection with the following fields:
		_id: owner,
		name:
		contact:
*/

var owner = ObjectId()

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09123456789"
})

/* Result: Robo3T
{
    "_id" : ObjectId("6255740ef7410cf4dd959291"),
    "name" : "John Smith",
    "contact" : "09123456789"
}
*/

db.suppliers.insert({
    name: "ABC Fruits",
    contact: "09123456789",
    owner_id: ObjectId("6255740ef7410cf4dd959291")
})

/* Result: Robo3T
{
    "_id" : ObjectId("62557655f7410cf4dd959292"),
    "name" : "ABC Fruits",
    "contact" : "09123456789",
    "owner_id" : ObjectId("6255740ef7410cf4dd959291")
}
*/

db.suppliers.insert({
    name:"DEF Fruits",
    contact:"09987654321",
    address:[
        {street: "123 San Jose St.", city: "Manila"},
        {street: "367 Gil Puyat", city:"Makati"}
    ]
})

/* Result: Robo3T
{
    "_id" : ObjectId("6255783ff7410cf4dd959293"),
    "name" : "DEF Fruits",
    "contact" : "09987654321",
    "address" : [ 
        {
            "street" : "123 San Jose St.",
            "city" : "Manila"
        }, 
        {
            "street" : "367 Gil Puyat",
            "city" : "Makati"
        }
    ]
}*/

var supplier = ObjectId()
var branch1 = ObjectId()
var branch2 = ObjectId()

db.suppliers.insert({
    _id: supplier,
    name:"GHI Fruits",
    contact:"09456789123",
    branches:[ branch1, branch2]
})

/* Result: Robo3T
{
    "_id" : ObjectId("6255799ef7410cf4dd959294"),
    "name" : "GHI Fruits",
    "contact" : "09456789123",
    "branches" : [ 
        ObjectId("6255799ef7410cf4dd959295"), 
        ObjectId("6255799ef7410cf4dd959296")
    ]
} */

/*
 _id:  ObjectId("6255799ef7410cf4dd959295") ; branch1 -> ObjectID
 _id:  ObjectId("6255799ef7410cf4dd959296") ; branch2 -> ObjectID
 supplier_id: ObjectId("6255799ef7410cf4dd959294") ; supplier
*/

db.branches.insertMany([
  {
  _id:  ObjectId("6255799ef7410cf4dd959295"),					
  name: "BF Homes",
  address: "123 Arcadio Santos St.",
  city: "parañaque",
  supplier_id: ObjectId("6255799ef7410cf4dd959294") 
  },
  {
  _id:  ObjectId("6255799ef7410cf4dd959296"),
  name: "Rizal",
  address: "123 San Jose St.",
  city: "Manila",
  supplier_id: ObjectId("6255799ef7410cf4dd959294")
  }   
])

/*	Result: Robo3T
{
    "_id" : ObjectId("6255799ef7410cf4dd959295"),
    "name" : "BF Homes",
    "address" : "123 Arcadio Santos St.",
    "city" : "parañaque",
    "supplier_id" : ObjectId("6255799ef7410cf4dd959294")
}

{
    "_id" : ObjectId("6255799ef7410cf4dd959296"),
    "name" : "Rizal",
    "address" : "123 San Jose St.",
    "city" : "Manila",
    "supplier_id" : ObjectId("6255799ef7410cf4dd959294")
}
*/


